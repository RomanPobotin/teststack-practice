﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStackPractice.TestHelpers;

namespace TestStackPractice.Tests
{
    [TestClass]
    public class Notepad
    {
        private readonly Applications _applications = new Applications();
        private readonly NotepadElements _notepadElements = new NotepadElements();
        private readonly string _directory = @"Y:\Documents\"; //Path to documents
        [TestMethod]
        public void EnterText()
        {
            var app = _applications._notepad;
            var neededWindow = _applications._notepadWindow;
            Application note = _applications.Start(app); //Start application method
            Window window = _applications.GetWindow(note, neededWindow); //Get main window method

            var textArea = _notepadElements.TextArea(window);
            var randomText = new RandomText().Text();
            textArea.Text = randomText;
            Assert.AreEqual(textArea.Text, randomText); //Check that text in notepad is correct

            var closeButton = _notepadElements.CloseButton(window);
            closeButton.Click();
            var closeWindow = _applications.CloseWindow(window);
            var save = _notepadElements.SaveButton(closeWindow);
            save.Click();

            var saveAsWindow = _applications.SaveWindow(window);
            var fileNameInput = _notepadElements.FileNameInput(saveAsWindow);

            var fileName = new DirectoryHelper().LastFile(_directory); 
            fileNameInput.Enter(fileName);
            var saveButton = _notepadElements.SaveAsButton(saveAsWindow);
            saveButton.Click();
            closeButton.Click();

            Assert.IsTrue(File.Exists(_directory+fileName)); //Check that file has been saved correctly
            _applications.Stop(note); //Close application method

        }
    }
}

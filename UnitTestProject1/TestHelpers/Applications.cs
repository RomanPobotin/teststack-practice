﻿using System;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace TestStackPractice.TestHelpers
{
    public class Applications
    {
        public readonly string _notepad = "notepad";
        public readonly string _notepadWindow = "Untitled - Notepad";

        public Application Start(string app)
        {
            Application note = Application.Launch(app);
            return note;
        }
        public Window GetWindow(Application app, string appWindow)
        {
            Window window = app.GetWindow(appWindow);
            return window;
        }
        public Window CloseWindow(Window window)
        {
            var closeWindow = window.ModalWindow("Notepad");
            return closeWindow;
        }
        public Window SaveWindow(Window window)
        {
            var saveAsWindow = window.ModalWindow("Save As");
            return saveAsWindow;
        }
        public void Stop(Application app)
        {
            try
            {
                app.Close();
            }
            catch (Exception e)
            {
                //Already closed
            }
            
        }
    }
}

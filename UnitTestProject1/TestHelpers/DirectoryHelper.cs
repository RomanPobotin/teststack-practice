﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TestStackPractice.TestHelpers
{
    public class DirectoryHelper
    {
        public string LastFile(string directory)
        {
            var files = Directory.GetFiles(directory, "*.txt", SearchOption.TopDirectoryOnly).ToList();
            var testFiles = new List<int>();
            foreach (var file in files)
            {
                var testFile = file.Replace("Y:\\Documents\\", "").Replace(".txt", "");
                try
                {
                    testFiles.Add(Convert.ToInt32(testFile));
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
            testFiles.Sort();
            testFiles.Reverse();
            var lastFile = testFiles.First() + 1;
            return lastFile + ".txt";
        }
    }
}

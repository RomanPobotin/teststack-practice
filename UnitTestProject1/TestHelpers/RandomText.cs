﻿using System;

namespace TestStackPractice.TestHelpers
{
    public class RandomText
    {
        public string Text()
        {
            var symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random rand = new Random();
            var stringLenght = rand.Next(0, symbols.Length);
            var randomText = "";
            for (int i = 0; i < stringLenght; i++)
            {
                randomText += symbols[rand.Next(0, stringLenght)];
            }
            return randomText;
        }
    }
}

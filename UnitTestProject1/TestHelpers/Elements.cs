﻿using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace TestStackPractice.TestHelpers
{
    public class NotepadElements
    {
        public TextBox TextArea(Window window)
        {
            return window.Get<TextBox>("15");
        }
        public Button CloseButton(Window window)
        {
            return window.Get<Button>("Close");
        }
        public Button SaveButton(Window window)
        {
            return window.Get<Button>("CommandButton_6");
        }
        public Button SaveAsButton(Window window)
        {
            return window.Get<Button>("1");
        }

        public UIItemContainer FileNameInput(Window window)
        {
            return window.MdiChild(SearchCriteria.ByAutomationId("1001"));
        }

    }
}
